---
title: "Optimize Communication"
description: "Learn about how to optimize communication with key players in Customer engagements."
---

- Executive
- Buyer
- Stakeholder
- Push communication - email status report
- Pull communication - post in Slack channel

Check w/ Dani A. on preferences
