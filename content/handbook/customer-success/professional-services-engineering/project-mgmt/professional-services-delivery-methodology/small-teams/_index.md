---
title: "Small Teams"
description: "Learn about the benefits of smaller team sizes."
---

Small teams communicate more efficiently, given the following conditions:

* team size \<= 7 team members - if the number of team members is, for example, 10, it is preferable to have two team of 5 each
* time zone aligned - if team members are disbursed across different times zones, communication slows down, almost in direct relationship to how many hours time difference folks experience; keeping team members in the same or close adjacent time zones simplifies communication
* clearly defined roles and responsibilities
* frequent in-person check-ins - highly recommended to do daily check-ins (aka stand-ups)
